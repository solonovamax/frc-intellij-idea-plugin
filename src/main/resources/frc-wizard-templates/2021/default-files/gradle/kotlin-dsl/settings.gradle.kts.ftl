<#ftl output_format="plainText" encoding="UTF-8"> <#-- Output formats: https://freemarker.apache.org/docs/dgui_misc_autoescaping.html -->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:    https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when luanching the testing instance of IntelliJ IDEA -->
</#compress>
import org.gradle.internal.os.OperatingSystem

pluginManagement {
repositories {
mavenLocal()
gradlePluginPortal()

val frcYear = "2020"
val frcHome: File
if (OperatingSystem.current().isWindows) {
var publicFolder = System.getenv("PUBLIC")
if (publicFolder == null) {
publicFolder = "C:\\Users\\Public"
}
val homeRoot = File(publicFolder, "wpilib")
frcHome = homeRoot.resolve(frcYear) // should be functionally equivalent to `File(homeRoot, frcYear)`
} else {
val userFolder = System.getProperty("user.home")
val homeRoot = File(userFolder, "wpilib")
frcHome = homeRoot.resolve(frcYear) // should be functionally equivalent to `File(homeRoot, frcYear)`
}

val frcHomeMaven = frcHome.resolve("maven") // should be functionally equivalent to `File(frcHome, "maven")`
maven {
name = "frcHome"
url = uri(frcHomeMaven)
}
}
}