<#ftl output_format="plainText" encoding="UTF-8"> <#-- Output formats: https://freemarker.apache.org/docs/dgui_misc_autoescaping.html -->
<#compress>
<#-- @ftlvariable name="data" type="net.javaru.iip.frc.wizard.FrcProjectWizardData" -->
<#--
    Template Language Reference: https://freemarker.apache.org/docs/ref.html
    Template Author's Guide:    https://freemarker.apache.org/docs/dgui.html
-->
<#--  To DEBUG templates, set system property 'frc.freemarker.debug' to true when luanching the testing instance of IntelliJ IDEA -->
</#compress>
import edu.wpi.first.gradlerio.frc.FRCJavaArtifact
import edu.wpi.first.gradlerio.frc.RoboRIO
import edu.wpi.first.toolchain.NativePlatforms
import jaci.gradle.deploy.artifact.ArtifactsExtension
import jaci.gradle.deploy.artifact.FileTreeArtifact
import jaci.gradle.deploy.target.TargetsExtension

plugins {
java
id("edu.wpi.first.GradleRIO").version("${data.wpilibVersion.versionString}")
}

java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

val mainClass = "${data.mainClassFQ}"

<#if !data.isRomiRobot()>
    // Define my targets (RoboRIO) and artifacts (deployable files)
    // This is added by GradleRIO's backing project EmbeddedTools.
    deploy {
    targets {
    this as TargetsExtension
    target("roborio", RoboRIO::class.java) {
    this as RoboRIO
    // Team number is loaded either from the .wpilib/wpilib_preferences.json
    // or from command line. If not found an exception will be thrown.
    // You can use getTeamOrDefault(team) instead of getTeamNumber if you
    // want to store a team number in this file.
    team = frc.teamNumber
    }
    }
    artifacts {
    this as ArtifactsExtension // hacky shit
    this.artifact("frcJava", FRCJavaArtifact::class.java) {
    targets.add("roborio")
    // Debug can be overridden by command line, for use with VSCode
    debug = frc.getDebugOrDefault(false)
    }
    this.artifact("frcStaticFileDeploy", FileTreeArtifact::class.java) {
    files.set(fileTree("src/main/deploy"))
    // Deploy to RoboRIO target, into /home/lvuser/deploy
    targets.add("roborio")
    directory = "/home/lvuser/deploy"
    }
    }
    }
</#if>

// Set this to true to enable desktop support.
val includeDesktopSupport = ${data.getIncludeDesktopSupportGradleSetting()}

<#if data.junitUseJUnitPlatform()>
    tasks.test {
    useJUnitPlatform()
    }
</#if>

dependencies {
<#if data.junitUseJUnitPlatform()>
    val junit5Version = "5.5.2"
</#if>
implementation(wpi.deps.wpilib())
<#if !data.isRomiRobot()>
    nativeZip(wpi.deps.wpilibJni(NativePlatforms.roborio))
</#if>
nativeDesktopZip(wpi.deps.wpilibJni(NativePlatforms.desktop))


implementation(wpi.deps.vendor.java())
<#if !data.isRomiRobot()>
    nativeZip(wpi.deps.vendor.jni(NativePlatforms.roborio))
</#if>
nativeDesktopZip(wpi.deps.vendor.jni(NativePlatforms.desktop))

<#if data.junitIsJUnit4Only()>
    testImplementation("junit:junit:4.13.1")
</#if>
<#if data.junitUseJUnitPlatform()>
    implementation(platform("org.junit:junit-bom:$junit5Version"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit5Version")
    testImplementation("org.junit.jupiter:junit-jupiter-params:$junit5Version")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junit5Version")
</#if>
<#if data.junitIncludeVintageSupport()>
    testImplementation("junit:junit:4.13.1")
    testRuntimeOnly("org.junit.vintage:junit-vintage-engine:4.13.1")
</#if>

// Enable simulation gui support. Must check the box in vscode to enable support
// upon debugging
simulation(wpi.deps.sim.gui(NativePlatforms.desktop, false))
simulation(wpi.deps.sim.driverstation(NativePlatforms.desktop, false))

// Websocket extensions require additional configuration.
// simulation wpi.deps.sim.ws_server(wpi.platforms.desktop, false)
<#if !data.isRomiRobot()>// </#if>simulation(wpi.deps.sim.ws_client(NativePlatforms.desktop, false))
}

<#if data.isRomiRobot()>
    // Set the websocket remote host (the Romi IP address).
    sim {
    envVar("HALSIMWS_HOST", "10.0.0.2")
    }
<#else>
    // Simulation configuration (e.g. environment variables).
    sim {
    // Sets the websocket client remote host.
    // envVar("HALSIMWS_HOST", "10.0.0.2")
    }
</#if>

// Setting up my Jar File. In this case, adding all libraries into the main jar ('fat jar')
// in order to make them all available at runtime. Also adding the manifest so WPILib
// knows where to look for our Robot Class.
tasks.jar {
from(configurations.runtimeClasspath.get().asFileTree)
manifest(edu.wpi.first.gradlerio.GradleRIOPlugin.javaManifest(mainClass))
}


/*
This is a certified b r u h moment.
see: https://github.com/gradle/gradle/issues/7649

Yes, this is a hacky workaround that I'm implementing. Do I look like I care?
why can't kotlin just have the same features as the groovy DLS...
*/
fun DependencyHandler.implementation(list: List${'<'}Any>) { <#-- shitty workaround. See: https://freemarker.apache.org/docs/dgui_misc_alternativesyntax.html -->
list.forEach { dependency ->
add("implementation", dependency)
}
}

fun DependencyHandler.nativeZip(list: List${'<'}Any>) {
list.forEach { dependency ->
add("nativeZip", dependency)
}
}

fun DependencyHandler.nativeDesktopZip(list: List${'<'}Any>) {
list.forEach { dependency ->
add("nativeDesktopZip", dependency)
}
}

fun DependencyHandler.simulation(list: List${'<'}Any>) {
list.forEach { dependency ->
add("simulation", dependency)
}
}