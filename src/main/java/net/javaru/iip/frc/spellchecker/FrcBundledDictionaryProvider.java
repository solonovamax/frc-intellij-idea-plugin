/*
 * Copyright 2015-2018 the original author or authors
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.spellchecker;

import com.intellij.spellchecker.BundledDictionaryProvider;


/**
 * Provides bundled dictionaries with FRC and WPI specific words.
 */
public class FrcBundledDictionaryProvider implements BundledDictionaryProvider
{
    // Class is configured in plugin.xml in:  <extensions><spellchecker.bundledDictionaryProvider>
    @Override
    public String[] getBundledDictionaries()
    {
        return new String[] {"frc-wpi.dic"};
    }
}
