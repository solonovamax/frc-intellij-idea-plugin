/*
 * Copyright 2015-2020 the original author or authors
 *
 *     Licensed under the Apache License, Version 2.0 (the "License");
 *     you may not use this file except in compliance with the License.
 *     You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *     
 *     Unless required by applicable law or agreed to in writing, software
 *     distributed under the License is distributed on an "AS IS" BASIS,
 *     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *     See the License for the specific language governing permissions and
 *     limitations under the License.
 */

package net.javaru.iip.frc.actions.create.advanced.cmdBased.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.swing.*;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.intellij.openapi.module.Module;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiModifier;
import com.intellij.ui.components.JBCheckBox;

import net.javaru.iip.frc.actions.create.advanced.ClassCreator;
import net.javaru.iip.frc.actions.create.advanced.FrcComponentCreationDataProvider;
import net.javaru.iip.frc.actions.create.advanced.cmdBased.FrcComponentCreationDialog;
import net.javaru.iip.frc.util.FrcClassUtils2;
import net.javaru.iip.frc.util.FrcClassUtilsKt;
import net.javaru.iip.frc.util.FrcCollectionExtsKt;
import net.javaru.iip.frc.util.FrcUiUtilsKt;
import net.javaru.iip.frc.util.PsiClassNameComparator;
import net.javaru.iip.frc.wpilib.WpiLibConstants;

import static net.javaru.iip.frc.i18n.FrcBundle.message;



public class CommandComponentCreationDialog extends FrcComponentCreationDialog
{
    private static final String SUBSYSTEMS_FQN_COMMA_DELIMITED_LIST = "requiredSubsystemsFqnCommaDelimitedString";
    private static final String SUBSYSTEMS_SIMPLE_NAME_COMMA_DELIMITED_LIST = "requiredSubsystemsNamesCommaDelimitedString";
    private static final String SUBSYSTEMS_VAR_NAME_COMMA_DELIMITED_LIST = "requiredSubsystemsVarsCommaDelimitedString";
    private static final String SUBSYSTEMS_SINGLETON_CALLS_COMMA_DELIMITED_LIST = "requiredSubsystemsSingletonCallsCommaDelimitedString";
    
    private Map<PsiClass, JBCheckBox> mySubsystemsClassesMap;
    
    
    public CommandComponentCreationDialog(@NotNull Module module,
                                          @NotNull ClassCreator classCreator,
                                          @NotNull PsiDirectory directory,
                                          @NotNull FrcComponentCreationDataProvider dataProvider)
    {
        super(module, classCreator, directory, dataProvider);
    }
    
    
    @Override
    protected void initMajorOptionsPanel(JPanel topPanel, JPanel optionsPanel)
    {
        if (includeSubsystemSelection())
        {
            // Display subsystems
            final String labelText = message("frc.new.class.adv.command.dialog.subsystems.label");
            List<PsiClass> subsystems = FrcClassUtilsKt.findImplementationsInModule(getSubsystemTopClassFQN(),
                                                                                    myModule,
                                                                                    false,
                                                                                    false,
                                                                                    null);
            // TODO: add option to show abstract subsystem classes... although I can't think of a use case for making an abstract subsystem a required subsystem of a command 
            
            //  hasModifier(JvmModifier.ABSTRACT)  is an experimental API. So for now we use the (older) hasModifierProperty(PsiModifier.ABSTRACT)
            //subsystems = subsystems.stream().filter(psiClass -> !psiClass.hasModifier(JvmModifier.ABSTRACT)).collect(Collectors.toList());
            subsystems = subsystems.stream().filter(psiClass -> !psiClass.hasModifierProperty(PsiModifier.ABSTRACT)).collect(Collectors.toList());
            subsystems = FrcClassUtilsKt.sortedByName(subsystems);
            
            // See our FrcClassUtilsKt.isAbstract() extension
            this.mySubsystemsClassesMap = FrcUiUtilsKt.initClassSelectionPanelCheckBoxes(topPanel,
                                                                                         optionsPanel,
                                                                                         labelText,
                                                                                         subsystems,
                                                                                         true);
        }
    }
    
    
    protected String getSubsystemTopClassFQN()
    {
        if (myDataProvider instanceof CommandCreationDataProvider)
        {
            return ((CommandCreationDataProvider) myDataProvider).getSubsystemTopFqName();
        }
        else
        {
            return WpiLibConstants.SUBSYSTEM_V2_TOP_FQN;
        }
    }
    
    protected boolean includeSubsystemSelection()
    {
        if (myDataProvider instanceof CommandCreationDataProvider)
        {
            return ((CommandCreationDataProvider) myDataProvider).getIncludeSubsystemChooser();
        }
        else
        {
            return true;
        }
    }
    
    @Override
    protected void addComponentSpecificProperties(@NotNull Builder<String, String> props,
                                                  @Nullable PsiClass baseClass,
                                                  @NotNull String targetPackageName,
                                                  @NotNull String newClassName)
    {
        addSubSystemProperties(props);
    }
    
    
    protected void addSubSystemProperties(@NotNull ImmutableMap.Builder<String, String> props)
    {
        final List<PsiClass> subsystems = getSelectedSubsystems();
        final String subSystemsFQN = FrcCollectionExtsKt.toCommaDelimitedString(subsystems, false, PsiClass::getQualifiedName);
        props.put(SUBSYSTEMS_FQN_COMMA_DELIMITED_LIST, subSystemsFQN);
        
        final String subSystemsSimpleNames = FrcCollectionExtsKt.toCommaDelimitedString(subsystems, false, PsiClass::getName);
        props.put(SUBSYSTEMS_SIMPLE_NAME_COMMA_DELIMITED_LIST, subSystemsSimpleNames);
        
        final String subSystemsVarNames = FrcCollectionExtsKt.toCommaDelimitedString(subsystems, false, psiClass ->
                StringUtils.uncapitalize(psiClass.getName()));
        props.put(SUBSYSTEMS_VAR_NAME_COMMA_DELIMITED_LIST, subSystemsVarNames);
    
        final List<String> singletonCalls = createSingletonCalls(subsystems);
        final String singletonCallsString = FrcCollectionExtsKt.toCommaDelimitedString(singletonCalls, false, String::toString, "<NULL>");
        props.put(SUBSYSTEMS_SINGLETON_CALLS_COMMA_DELIMITED_LIST, singletonCallsString);
    }
    
    @NotNull
    protected List<String> createSingletonCalls(@NotNull List<PsiClass> classes)
    {
        final List<String> calls = new ArrayList<>(classes.size());
        for (PsiClass psiClass : classes)
        {
            final String singletonCall = createSingletonCall(psiClass);
            calls.add(singletonCall);
        }
        return calls;
    }
    
    
    @Nullable
    private static String createSingletonCall(PsiClass psiClass)
    {
        String result = null;
        if (FrcClassUtils2.isSingleton(psiClass))
        {
            // favor using the getter if it is present
            final PsiMethod singletonMethod = FrcClassUtils2.getSingletonMethod(psiClass);
            if (singletonMethod != null)
            {
                result = psiClass.getName() + "." + singletonMethod.getName() + "()";
            }
            else
            {
                final PsiField singletonField = FrcClassUtils2.getSingletonField(psiClass);
                if (singletonField != null && singletonField.hasModifierProperty(PsiModifier.PUBLIC))
                {
                    result = psiClass.getName() + "." + singletonField.getName();
                }
            }
        }
        return result;
    }
    
    
    protected List<PsiClass> getSelectedSubsystems()
    {
        final List<PsiClass> subsystems = new ArrayList<>();
        if (includeSubsystemSelection())
        {
            for (Entry<PsiClass, JBCheckBox> entry : mySubsystemsClassesMap.entrySet())
            {
                if (entry.getValue().isSelected())
                {
                    subsystems.add(entry.getKey());
                }
            }
            subsystems.sort(PsiClassNameComparator.INSTANCE);
        }
        return subsystems;
    }
}
